"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const mongodb_1 = require("mongodb");
const Schema = mongoose_1.default.Schema;
const AlbumSchema = new Schema({
    title: String,
    description: String,
    year: Number,
    image: String,
    artist: { type: mongodb_1.ObjectId, ref: 'Artist' }
});
exports.default = mongoose_1.default.model('Album', AlbumSchema, 'Albums');
