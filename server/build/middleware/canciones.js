"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const multer_1 = __importDefault(require("multer"));
const songModel_1 = __importDefault(require("../models/songModel"));
const fs_1 = __importDefault(require("fs"));
const images_1 = __importDefault(require("./images"));
class cancionesMiddleware {
    cargar(req, file, callBack) {
        var songId = req.params.id;
        songModel_1.default.findById(songId).populate({ path: 'album', populate: { path: 'artist', model: 'Artist' } }).exec((err, song) => {
            var dir = `D:/gitKraken/Ionic/mean/server/src/public/uploads/artists/${song.album.artist._id}/${song.album.title}/`;
            console.log(dir);
            if (!fs_1.default.existsSync(dir)) {
                console.log('mkdir 1');
                fs_1.default.mkdirSync(dir, { recursive: true });
                console.log('mkdir finish');
            }
            var upload = multer_1.default({
                storage: multer_1.default.diskStorage({
                    destination: (req, file, callBack) => {
                        callBack(null, dir);
                    },
                    filename: (req, file, callBack) => {
                        var ext = file.originalname.split('\.');
                        var fileExt = ext[1];
                        var newName = req.body.number + '-' + req.body.name + '.' + fileExt;
                        callBack(null, `${newName}`);
                    },
                }),
                fileFilter: (req, file, callBack) => {
                    var ext = file.originalname.split('\.');
                    var fileExt = ext[1];
                    if (fileExt == 'mp3') {
                        callBack(null, true);
                    }
                    else {
                        req.fileValidationError = 'el archivo no es del tipo esperado';
                        callBack(new Error('el archivo no es del tipo esperado'), false);
                    }
                }
            }).single('song');
        });
        return images_1.default;
    }
}
exports.default = new cancionesMiddleware();
