"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class IndexController {
    index(req, res) {
        res.status(200).json({ text: 'HELLO' });
    }
}
const indexController = new IndexController();
exports.default = indexController;
