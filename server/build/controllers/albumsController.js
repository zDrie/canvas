"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const albumModel_1 = __importDefault(require("../models/albumModel"));
class AlbumsController {
    getAll(req, res) {
        var artistId = req.params.artist;
        var page = req.params.page || 1;
        var itemsPerPage = 4;
        if (!artistId) {
            albumModel_1.default
                .find({})
                .populate({ path: 'artist' })
                .sort('title')
                .skip((itemsPerPage * page) - itemsPerPage)
                .limit(itemsPerPage)
                .exec((err, albums) => {
                if (err) {
                    res.status(500).send('hubo un error en la petición');
                }
                else {
                    if (albums) {
                        albumModel_1.default.estimatedDocumentCount((err, count) => {
                            if (err) {
                                res.status(404).send('no hay albums');
                            }
                            else {
                                var pages = Math.ceil(count / itemsPerPage);
                                res.status(200).send({
                                    pages: pages,
                                    albums: albums
                                });
                            }
                        });
                    }
                    else {
                        res.status(404).send('no se han podido obtener los albums');
                    }
                }
            });
        }
        else {
            albumModel_1.default
                .find({ artist: artistId })
                .populate({ path: 'artist' })
                .sort('year')
                .skip((itemsPerPage * page) - itemsPerPage)
                .limit(itemsPerPage)
                .exec((err, albums) => {
                if (err) {
                    res.status(500).send('hubo un error en la petición');
                }
                else {
                    if (albums) {
                        albumModel_1.default.estimatedDocumentCount((err, count) => {
                            if (err) {
                                res.status(404).send('no hay albums');
                            }
                            else {
                                var pages = Math.ceil(count / itemsPerPage);
                                res.status(200).send({
                                    pages: pages,
                                    albums: albums
                                });
                            }
                        });
                    }
                    else {
                        res.status(404).send('no se han podido obtener los albums');
                    }
                }
            });
        }
    }
    getOne(req, res) {
        var albumId = req.params.id;
        albumModel_1.default.findById(albumId).populate({ path: 'artist' }).exec((err, album) => {
            if (err) {
                res.status(500).send('Error en la petición');
            }
            else {
                if (album) {
                    res.status(200).send(album);
                }
                else {
                    res.status(404).send('el album no existe');
                }
            }
        });
    }
    saveAlbum(req, res) {
        var album = new albumModel_1.default();
        var params = req.body;
        album.title = params.title;
        album.description = params.description;
        album.year = params.year;
        album.image = 'null';
        album.artist = params.artist;
        album.save((err, albumStored) => {
            if (err) {
                res.status(500).send('error al guardar el album');
            }
            else {
                if (albumStored) {
                    res.status(200).send(albumStored);
                }
                else {
                    res.status(404).send('el album no ha sido guardado');
                }
            }
        });
    }
    updateAlbum(req, res) {
        var albumID = req.params.id;
        var title = req.body.title;
        var description = req.body.description;
        var year = req.body.year;
        var artist = req.body.artist;
        var image = req.body.image;
        albumModel_1.default.findByIdAndUpdate(albumID, { $set: { 'title': title, 'year': year, 'artist': artist, 'description': description, 'image': image } }, { new: true }, (err, albumUpdated) => {
            if (err) {
                res.status(500).send('error al actualizar el album');
                throw err;
            }
            else {
                if (!albumUpdated) {
                    res.status(404).send('no se ha podido actualizar el album');
                }
                else {
                    res.status(200).send(albumUpdated);
                }
            }
        });
    }
    deleteAlbum(req, res) {
        var albumID = req.params.id;
        albumModel_1.default.findByIdAndRemove(albumID, (err, albumRemoved) => {
            if (err) {
                res.status(500).send('error al eliminar el album');
            }
            else {
                if (albumRemoved) {
                    albumModel_1.default.find({ 'artist': albumRemoved._id }).remove((err, songRemoved) => {
                        if (err) {
                            res.status(500).send('error al eliminar la canción');
                        }
                        else {
                            if (songRemoved) {
                                res.status(200).send(songRemoved);
                            }
                            else {
                                res.status(404).send('la canción no ha sido eliminada');
                            }
                        }
                    });
                }
                else {
                    res.status(404).send('el album no ha sido eliminado');
                }
            }
            res.status(200).send(albumRemoved);
        });
    }
    uploadImage(req, res) {
        if (req.file) {
            if (req.file)
                albumModel_1.default.findByIdAndUpdate(req.params.id, { 'image': req.file.filename }, { new: true }, (err, albumUpdated) => {
                    if (albumUpdated) {
                        res.status(200).send(albumUpdated);
                    }
                    else {
                        res.status(404).send('se ha subido la imagen pero no se ha podido actualizar el usuario');
                    }
                });
        }
        else {
            res.status(200).send('no se ha subido ninguna imagen');
        }
    }
    getImage(req, res) {
        var imagefile = req.params.imageFile;
        const file = './src/public/uploads/albums/images/' + imagefile;
        fs_1.default.exists(file, (exists) => {
            if (exists) {
                res.status(200).sendFile(path_1.default.resolve(file));
            }
            else {
                res.status(404).send('no existe la imagen');
            }
        });
    }
}
const albumsController = new AlbumsController();
exports.default = albumsController;
