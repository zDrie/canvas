"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const albumsController_1 = __importDefault(require("../controllers/albumsController"));
const express_1 = require("express");
const autenticated_1 = __importDefault(require("../middleware/autenticated"));
const albumsImages_1 = __importDefault(require("../middleware/albumsImages"));
class ArtistsRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        //get
        this.router.get('/all/:page?/:artist?', albumsController_1.default.getAll);
        this.router.get('/:id', albumsController_1.default.getOne);
        this.router.get('/get-image/:imageFile', albumsController_1.default.getImage);
        //post
        this.router.post('/', albumsController_1.default.saveAlbum);
        this.router.post('/upload-image/:id', albumsImages_1.default, albumsController_1.default.uploadImage);
        //put
        this.router.put('/:id', albumsController_1.default.updateAlbum);
        //delete
        this.router.delete('/:id', autenticated_1.default, albumsController_1.default.deleteAlbum);
    }
}
const albumsRouter = new ArtistsRoutes();
exports.default = albumsRouter.router;
