"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const artistsController_1 = __importDefault(require("../controllers/artistsController"));
const express_1 = require("express");
const autenticated_1 = __importDefault(require("../middleware/autenticated"));
const artistImages_1 = __importDefault(require("../middleware/artistImages"));
class ArtistsRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        //get
        this.router.get('/all/:page?', artistsController_1.default.getAll);
        this.router.get('/:id', artistsController_1.default.getOne);
        this.router.get('/get-image/:imageFile', artistsController_1.default.getImage);
        //post
        this.router.post('/', autenticated_1.default, artistsController_1.default.saveArtist);
        this.router.post('/upload-image/:id', artistImages_1.default, artistsController_1.default.uploadImage);
        //put
        this.router.put('/:id', autenticated_1.default, artistsController_1.default.updateArtist);
        //delete
        this.router.delete('/:id', autenticated_1.default, artistsController_1.default.deleteArtist);
    }
}
const artistsRouter = new ArtistsRoutes();
exports.default = artistsRouter.router;
