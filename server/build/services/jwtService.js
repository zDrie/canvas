"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt_simple_1 = __importDefault(require("jwt-simple"));
const moment_1 = __importDefault(require("moment"));
//no tiene sentido que esto este aca
var secret = 'clave_secreta_curso';
function createToken(user) {
    var payload = {
        sub: user._id,
        name: user.name,
        surname: user.surname,
        email: user.email,
        role: user.role,
        image: user.image,
        iat: moment_1.default().unix(),
        exp: moment_1.default().add(30, 'days').unix()
    };
    return jwt_simple_1.default.encode(payload, secret);
}
exports.default = createToken;
