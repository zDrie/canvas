import express, { Application } from 'express';
import colors from 'colors';
import morgan from 'morgan';
import mongoose from 'mongoose';
import indexRoutes from './routes/indexRoutes';
import usersRoutes from './routes/usersRoutes';
import artistsRoutes from './routes/artistsRoutes';
import albumsRoutes from './routes/albumsRoutes';
import songsRoutes from './routes/songsRoutes';
import { urlencoded } from 'body-parser';
import { MongoError } from 'mongodb';


class Server {
    public app: Application;

    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }
    config(): void {
        this.app.set('port', process.env.PORT || 3000);
        this.app.use(morgan('dev'));
        this.app.use(express.json());
        this.app.use(urlencoded({ extended: false }));
        this.app.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            req.header('Access-Control-Allow-Origin');
            res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
            res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
            res.header('Allow','GET, POST, OPTIONS, PUT, DELETE');
            next();
        })
    }

    routes(): void {
        this.app.use('/', indexRoutes);
        this.app.use('/api/users', usersRoutes);
        this.app.use('/api/artists', artistsRoutes);
        this.app.use('/api/albums', albumsRoutes);
        this.app.use('/api/songs', songsRoutes);

    }


    //conectando a la base de datos

    start(): void {
        const connectDb = () => {
            return mongoose.connect('mongodb://localhost:27017/cursomean', {
                useNewUrlParser: true,
                useFindAndModify: false,
                useUnifiedTopology: true
            }, (err: MongoError) => {
                if (err) {
                    throw err
                } else {
                    console.log('la base de datos está conectada'.america.inverse.italic);
                }
            })
        }

        connectDb().then(() => {
            this.app.listen(this.app.get('port'), () => {
                console.log(colors.random(`Server ON Port ${this.app.get('port')}`))
            })
        })

    }
}

const server = new Server();
server.start();





