import albumsController from '../controllers/albumsController';
import { Router } from 'express';
import md_auth from '../middleware/autenticated';
import upload from '../middleware/albumsImages'

class ArtistsRoutes{
    public router: Router = Router();

    constructor() {
        this.config();
    }
    config(): void {
        //get
        this.router.get('/all/:page?/:artist?', albumsController.getAll);
        this.router.get('/:id', albumsController.getOne);
        this.router.get('/get-image/:imageFile', albumsController.getImage);

        //post
        this.router.post('/', albumsController.saveAlbum);
        this.router.post('/upload-image/:id', upload, albumsController.uploadImage);

        //put
        this.router.put('/:id', albumsController.updateAlbum);

        //delete
        this.router.delete('/:id', md_auth, albumsController.deleteAlbum);
    }

}

const albumsRouter = new ArtistsRoutes();
export default albumsRouter.router;
