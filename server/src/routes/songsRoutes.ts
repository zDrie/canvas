import songController from '../controllers/songsController';
import { Router } from 'express';
import md_auth from '../middleware/autenticated';
import upload from '../middleware/songs'

class ArtistsRoutes{
    public router: Router = Router();

    constructor() {
        this.config();
    }
    config(): void {
        //get
        this.router.get('/all/:album?', songController.getAll);
        this.router.get('/:id', songController.getOne);
        this.router.get('/get-file/:id', songController.getAudio);

        //post
        this.router.post('/', songController.saveSong);
        this.router.post('/upload-file/:id', upload, songController.uploadAudio);

        //put
        this.router.put('/:id', md_auth, songController.updateSong);

        //delete
        this.router.delete('/:id', md_auth, songController.deleteSong);
    }

}

const albumsRouter = new ArtistsRoutes();
export default albumsRouter.router;
