import { Router } from 'express';
import usersController from '../controllers/usersController';
import md_auth from '../middleware/autenticated';
import upload from '../middleware/images';


class UsersRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    config(): void {


        //obtener
        this.router.get('/probando', md_auth, usersController.index);
        this.router.get('/get-avatar/:imageFile', usersController.getAvatar);

        //crear
        this.router.post('/register', usersController.saveUser);
        this.router.post('/login', usersController.loginUser);
        this.router.post('/upload-image/:id', md_auth, function(req, res, next){
            upload(req, res, function(err){
                if (req.fileValidationError){
                    return res.status(500).end('el archivo no es del tipo esperado');
                }else{
                    next();
                }
                
            })
        }, usersController.uploadImage);

        //modificar
        this.router.put('/update/:id', md_auth, usersController.updateUser);
        //eliminar
        this.router.delete('/:id', md_auth, usersController.index);


    }
}

const usersRoutes = new UsersRoutes();
export default usersRoutes.router;