import path from 'path';
import fs from 'fs';
import {Request, Response} from 'express';
import Artist from '../models/artistModel';
import Album from '../models/albumModel';
import Song from '../models/songModel';

class ArtistsController{
    getAll(req:Request, res:Response){
        var page = req.params.page || 1;
        var itemsPerPage = 4;
        Artist
        .find({})
        .sort('name')
        .skip((itemsPerPage * page) - itemsPerPage)
        .limit(itemsPerPage)
        .exec((err, artists)=>{
            if(err){
                res.status(500).send('hubo un error en la petición')
            }else{
                if(artists){
                    Artist.estimatedDocumentCount((err:Error, count)=>{
                        if (err){
                            res.status(404).send('no hay artistas');
                        }else{
                            var pages = Math.ceil(count/itemsPerPage)
                            res.status(200).send({
                                pages: pages,
                                artists: artists
                            });
                        }
                    })
                }else{
                    res.status(404).send('no se han podido obtener los artistas')
                }
            }
        });
        

    }
    
    getOne(req:Request, res:Response){
        var artistId = req.params.id;
        Artist.findById(artistId, (err, artist)=>{
            if(err){
                res.status(500).send('Error en la petición')
            }else{
                if(artist){
                    res.status(200).send(artist);
                }else{
                    res.status(404).send('el artista no existe')
                }

            }
        })
    }

    saveArtist(req:Request, res:Response){
        var artist = new Artist();
        var params = req.body;
        artist.name = params.name;
        artist.description = params.description;
        artist.image = 'music.png';

        artist.save((err: any, artistStored: any)=>{
            if (err){
                res.status(500).send('error al guardar el artista');
            }else{
                if(artistStored){
                    res.status(200).send(artistStored);
                }else{
                    res.status(404).send('el artista no ha sido guardado');
                }
            }
        })
    }

    updateArtist(req:Request, res:Response) {
        var artID = req.params.id;
        var name = req.body.name;
        var description = req.body.description;
        var image = req.body.image;
        Artist.findByIdAndUpdate(artID, {$set:{'name': name, 'description':description,'image': image}},{new:true}, (err, artistUpdated)=>{
            if (err){
                res.status(500).send('error al actualizar el artista')
                throw err;
            }else{
                if(!artistUpdated){
                    res.status(404).send('no se ha podido actualizar el artista')
                }else{
                    res.status(200).send(artistUpdated);
                }
            }
        });
    }

    deleteArtist(req:Request, res:Response){
        var artID = req.params.id;
        Artist.findByIdAndRemove(artID, (err, artRemoved)=>{
            if(err){
                res.status(500).send('error al eliminar el artista');
            }else{
                if(artRemoved){
                    Album.find({'artist': artRemoved._id}).remove((err:Error, albRemoved: any)=>{
                        if (err){
                            res.status(500).send('error al eliminar el album');
                        }else{
                            if(albRemoved){
                                res.status(200).send(albRemoved);
                                Song.find({'album': albRemoved._id}).remove((err:Error, songRemoved: any)=>{
                                    if(err){
                                        res.status(500).send('error al eliminar la canción')
                                    }else{
                                        if(songRemoved){
                                            res.status(200).send(songRemoved);
                                        }else{
                                            res.status(404).send('la canción no ha sido eliminada');
                                        }
                                    }
                                })
                            }else{
                                res.status(404).send('el album no ha sido eliminad')
                            }
                        }
                    });
                    res.status(200).send(artRemoved);
                }else{
                    res.status(404).send('el artista no se ha eliminado');
                }
            }
        });
    }

    uploadImage(req:Request, res:Response){

        if(req.file){
           if(req.file)
           Artist.findByIdAndUpdate(req.params.id, {'image': req.file.filename}, {new: true}, (err, artistUpdated)=>{
            if (artistUpdated){
            res.status(200).send(artistUpdated);
            }else{
                res.status(404).send('se ha subido la imagen pero no se ha podido actualizar el usuario')
            }
        });
        }else{
            res.status(200).send('no se ha subido ninguna imagen');
        }
    }

    getImage(req:Request, res:Response){
        var imagefile = req.params.imageFile;
        const file = './src/public/uploads/artists/images/'+imagefile;
        console.log(file);
    
        fs.exists(file, (exists)=>{
            if (exists){
                res.status(200).sendFile(path.resolve(file))
            }else{
                res.status(404).send('no existe la imagen')
            }
        });
    }

}


const artistsController = new ArtistsController();
export default artistsController;