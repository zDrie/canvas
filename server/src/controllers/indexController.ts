import {Request, Response} from 'express'

class IndexController{

    index(req:Request, res:Response){
        res.status(200).json({text: 'HELLO'})
    }
}
const indexController = new IndexController();
export default indexController;
