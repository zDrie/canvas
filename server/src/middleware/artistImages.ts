import multer from 'multer';



var storage1 = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, './src/public/uploads/artists/images')
    },
    filename: (req, file, callBack) => {
        var ext = file.originalname.split('\.');
        var fileExt = ext[1];
        var newName = req.params.id + '.' + fileExt;
        callBack(null, `image_${newName}`)

    },
})

var upload = multer({
    storage: storage1,
    fileFilter: (req, file, callBack)=> {
        var ext = file.originalname.split('\.');
        var fileExt = ext[1];
        if(fileExt == 'png' || fileExt == 'jpg'|| fileExt == 'gif'){
            return callBack(null, true);
        }else{
            req.fileValidationError = 'el archivo no es del tipo esperado'
            return callBack(new Error('el archivo no es del tipo esperado'), false);
        }
    }
}).single('image');

export default upload;