import multer from 'multer';
import Song from '../models/songModel';
import { Request, Response } from 'express';
import fs from 'fs';
import upload from './images';

class cancionesMiddleware {

    cargar(req: Request, file: Response, callBack) {
        var songId = req.params.id;
        Song.findById(songId).populate({ path: 'album', populate: { path: 'artist', model: 'Artist' } }).exec((err, song) => {
            var dir = `D:/gitKraken/Ionic/mean/server/src/public/uploads/artists/${song.album.artist._id}/${song.album.title}/`;
            console.log(dir);
        
            if (!fs.existsSync(dir)) {
                console.log('mkdir 1');
                fs.mkdirSync(dir, {recursive:true});
                console.log('mkdir finish');
            }
           var upload = multer({
                storage: multer.diskStorage({
                    destination: (req, file, callBack) => {
                        callBack(null, dir);
                    },
                    filename: (req, file, callBack) => {
                        var ext = file.originalname.split('\.');
                        var fileExt = ext[1];
                        var newName = req.body.number + '-' + req.body.name + '.' + fileExt;
                        callBack(null, `${newName}`);
                    },
                }),
                fileFilter: (req, file, callBack) => {
                    var ext = file.originalname.split('\.');
                    var fileExt = ext[1];
                    if (fileExt == 'mp3') {
                         callBack(null, true);
                    } else {
                        req.fileValidationError = 'el archivo no es del tipo esperado'
                         callBack(new Error('el archivo no es del tipo esperado'), false);
                    }
                }
            }).single('song');
        });
        return upload;
    }
}


export default new cancionesMiddleware();