import multer from 'multer';
import Song from '../models/songModel';
import fs from 'fs';

var dirname = '';
var newName = '';

var upload = multer({
    storage: multer.diskStorage({
        destination: (req, file, callBack) => {
            var songId = req.params.id;
            Song.findById(songId).populate({ path: 'album', populate: { path: 'artist', model: 'Artist' } }).exec((err, song) => {
                dirname = `./src/public/uploads/artists/${song.album.artist.name}/${song.album.title}/`;
                if (!fs.existsSync(dirname)) {
                    fs.mkdirSync(dirname, { recursive: true });
                }
            });
            callBack(null, dirname);
        },
        filename: (req, file, callBack) => {
            var songId = req.params.id;
            Song.findById(songId).exec((err, song) => {
                var ext = file.originalname.split('\.');
                var fileExt = ext[1];
                newName = song.number + '-' + song.name + '.' + fileExt;
            });
            console.log(newName);
            callBack(null, `${newName}`);
        },
    }),
    fileFilter: (req, file, callBack) => {
        var ext = file.originalname.split('\.');
        var fileExt = ext[1];

        if (fileExt == 'mp3' || fileExt == 'wma' || fileExt == 'wav' || fileExt == 'mp4' ) {
            return callBack(null, true);
        } else {
            req.fileValidationError = 'el archivo no es del tipo esperado'
            return callBack(new Error('el archivo no es del tipo esperado'), false);
        }
    }
}).single('file');


export default upload;