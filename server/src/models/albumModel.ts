import mongoose from 'mongoose';
import { ObjectId } from 'mongodb';

const Schema = mongoose.Schema;

const AlbumSchema = new Schema ({
    title: String,
    description: String,
    year: Number,
    image: String,
    artist: { type: ObjectId, ref: 'Artist'}
});

export default mongoose.model('Album', AlbumSchema, 'Albums' );