import mongoose from 'mongoose';
import { ObjectId } from 'mongodb';

const Schema = mongoose.Schema;

const SongSchema = new Schema ({
    number: String,
    name: String,
    duration: String,
    file: String,
    album: { type: ObjectId, ref: 'Album'}
});

export default mongoose.model('Song', SongSchema, 'Songs' );