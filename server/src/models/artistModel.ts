import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ArtistSchema = new Schema ({
    name: String,
    description: String,
    image: String
});

const Artist = mongoose.model('Artist', ArtistSchema, 'Artists');
export default Artist;